<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = ['model','year','gallery','user_id','rented_at','free_from'];
}
